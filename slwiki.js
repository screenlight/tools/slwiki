const url = require('url');
const http = require('http');
const https = require('https');
const querystring = require('querystring');

module.exports = {
    connect: function(config){
        var baseURLData = url.parse(config.url);
        var ssl = baseURLData.protocol == 'https:';
        var client = (ssl?https:http);
        
        var token = null;
        var cookies = {};

        var send = async function(gets,post){ return new Promise( (resolve,reject) => {
            if ( !gets ) gets = {};
            if ( !gets.format ) gets.format = 'json';
            if ( token ) gets.token = token;

            var options = {
                host:baseURLData.host,
                port:baseURLData.port,
                protocol:baseURLData.protocol,
                path: baseURLData.path +'api.php'+(gets?'?'+querystring.stringify(gets):''),
                method:'GET',
                headers:{}
            };
            
            var cookiestring = '';
            for ( name in cookies ) {
                cookiestring += name +'='+cookies[name]+';';
            }

            if ( cookiestring ) options.headers['Cookie'] = cookiestring;

            var postData;

            if ( post ) {
                options.method = 'POST';
                postData = querystring.stringify(post);

                options.headers['Content-Type'] = 'application/x-www-form-urlencoded';
                options.headers['Content-Length'] = Buffer.byteLength(postData);
            }
            
            var req = client.request(options, response => {
                var data = '';
        
                response.setEncoding('utf-8');
    
                response.on('data', function(chunk){
                    data += chunk;
                });
    
                response.on('end', function() {
                    if ( response.statusCode == 200 ) {
                        if ( response.headers['set-cookie'] ) {
                            response.headers['set-cookie'].forEach(function(cookie){
                                var match = cookie.match(/([^;]+)=([^;]+)/);
    
                                if ( match ) cookies[match[1]] = match[2];
                            });
                        }

                        resolve(JSON.parse(data));
                    } else {
                        reject(response.statusCode);
                    }
                });
            });

            req.on('error', error => {
                console.error(error);
                reject(error);
            });

            if ( postData ) req.write(postData);
            req.end();
        })};

        var connected = new Promise( (resolve,reject) => {
            send({
                action:'query',
                meta:'tokens',
                type:'login'
            },{}).then( response => {
                var loginToken = response.query.tokens.logintoken;

                send({
                    action:'login',
                },{
                    lgtoken:loginToken,
                    lgname:config.user,
                    lgpassword:config.password
                }).then( response => {
                    token = response.login.token;

                    resolve();
                })
            }).catch( error => {
                console.error('failed to connect to wiki',error);
                reject();
            })
        });

        return {
            ask: async function(query){ return new Promise( (resolve,reject) => {
                connected.then(() => {
                    send({
                        action:'ask',
                        query:query
                    },{token:token}).then(data => {
                        var result = {
                            list:[],
                        };

                        if ( data.query ) {
                            for ( var key in data.query.results ) {
                                var object = data.query.results[key].printouts;
                                object.id = data.query.results[key].fulltext;

                                result.list.push(object);
                            }
                        }

                        //result.list = result.list.slice(0,10)

                        resolve(result);
                    })
                }).catch(error => {
                    resolve({
                        error:error,
                        list:[],
                    });
                })
            })}
        }
    }
}